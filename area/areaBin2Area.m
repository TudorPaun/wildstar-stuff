function area = areaBin2Area()
    name = 'eastern.3546.area';
    
    % ##################################
    % READ FILE
    % ##################################
    fileId = fopen(name);
    binaryStream = fread(fileId);
    fclose(fileId);
    
    % ##################################
    % INITIALIZE
    % ##################################
    masks;
    
    name = strsplit(name, '\');
    name = name{end};
    
    area = struct();
    area.chunk(1) = struct();
    area.prop(1) = struct();
    area.name = name;

    
    % ##################################
    % DECODE HEADER
    % ##################################
    magic = char(binaryStream(1:4)');
    binaryStream(1:4) = [];
    version = char(binaryStream(1:4)');
    binaryStream(1:4) = [];

    chnk = char(binaryStream(1:4)');
    binaryStream(1:4) = [];
    chnk_length = bi2de(binaryStream(1:4)',256);
    binaryStream(1:4) = [];
    chunks = binaryStream(1:chnk_length);
    binaryStream(1:chnk_length) = [];

    if length(binaryStream)>4
        prop = char(binaryStream(1:4)');
        binaryStream(1:4) = [];
        prop_length = bi2de(binaryStream(1:4)',256);
        binaryStream(1:4) = [];
        props = binaryStream(1:prop_length);
        binaryStream(1:prop_length) = [];
    end

    % ##################################
    % DECODE CHUNKS
    % ##################################
    pos = 1;
    for chk = 1:256
        try
            chunk_size = bi2de(chunks(pos:pos+3)',256);
            pos = pos + 3;
            chunk_size = bitand(chunk_size, hex2dec(MASK.size));
            chunk_data = chunks(pos+1:pos+chunk_size);
            pos = pos + chunk_size + 1;

            flags = bi2de(chunk_data(1:4)',256);
            chunk_data(1:4) = [];
            if bitand(flags,hex2dec(MASK.height))
                height = bi2de(reshape(chunk_data(1:19*19*2),2,[])', 256)';
                height = reshape(height,19,19)';
                chunk_data(1:19*19*2) = [];
                area.chunk(chk).height = uint16(height);
            end
            if bitand(flags,hex2dec(MASK.texture_id))
                texture_ids = zeros(1,4);
                for i = 1:4
                    texture_ids(i) = bi2de(chunk_data(1:4)',256);
                    chunk_data(1:4) = [];
                end
                area.chunk(chk).texture_ids = texture_ids;
            else
                area.chunk(chk).texture_ids = [];
            end
            if bitand(flags,hex2dec(MASK.texture_blend))
                texture_blend = bi2de(reshape(chunk_data(1:65*65*2),2,[])', 256)';
                texture_blend = reshape(texture_blend,65,65)';
                chunk_data(1:65*65*2) = [];
                area.chunk(chk).texture_blend = uint16(texture_blend);
            else
                area.chunk(chk).texture_blend = [];
            end
            if bitand(flags,hex2dec(MASK.color_map))
                color_map = bi2de(reshape(chunk_data(1:65*65*2),2,[])', 256)';
                color_map = reshape(color_map,65,65)';
                chunk_data(1:65*65*2) = [];
                area.chunk(chk).color_map = uint16(color_map);
            else
                area.chunk(chk).color_map = [];
            end
            if bitand(flags,hex2dec(MASK.unk1)) || bitand(flags,hex2dec(MASK.unk1b))
                unk1 = chunk_data(1:80)';
                chunk_data(1:80) = [];
                area.chunk(chk).unk1 = unk1;
            else
                area.chunk(chk).unk1 = [];
            end
            if bitand(flags,hex2dec(MASK.shadow_map))
                shadow_map = bi2de(chunk_data(1:65*65), 256)';
                shadow_map = reshape(shadow_map,65,65)';
                chunk_data(1:65*65) = [];
                area.chunk(chk).shadow_map = uint8(shadow_map);
            else
                area.chunk(chk).shadow_map = [];
            end
            if bitand(flags,hex2dec(MASK.unk2))
                % stores a map. strange values as chunks seem to change
                % color/height suddenly. also, best results obtained if
                % only using unk2(5:64,5:64)
                unk2 = bi2de(chunk_data(1:64*64), 256)';
                unk2 = reshape(unk2,64,64)';
                chunk_data(1:64*64) = [];
                area.chunk(chk).unk2 = uint8(unk2);
            else
                area.chunk(chk).unk2 = [];
            end
            if bitand(flags,hex2dec(MASK.unk3))
                unk3 = bi2de(chunk_data(1), 256)';
                chunk_data(1) = [];
                area.chunk(chk).unk3 = uint8(unk3);
            else
                area.chunk(chk).unk3 = [];
            end
            if bitand(flags,hex2dec(MASK.unk4))
                % ???
                disp('found unk4!');
            else
                area.chunk(chk).unk4 = [];
            end
            if bitand(flags,hex2dec(MASK.unk5))
                area.chunk(chk).unk5 = chunk_data(1:21316);
                chunk_data(1:21316) = [];
            else
                area.chunk(chk).unk5 = [];
            end
            if bitand(flags,hex2dec(MASK.unk6))
                % ???
                disp('found unk6!');
            else
                area.chunk(chk).unk6 = [];
            end
            if bitand(flags,hex2dec(MASK.unk7))
                % unk7 seems to be inconsistent in size. ranging from
                % 16-84. maybe more. most of the times it is of size 16
%                 size(chunk_data)
            else
                area.chunk(chk).unk7 = [];
            end
            if bitand(flags,hex2dec(MASK.unk8))
                % ???
                disp('found unk8!');
            else
                area.chunk(chk).unk8 = [];
            end
            % READ MASK UNK 4-8. STILL NOT IMPLEMENTED
        catch
            area.chunk(chk).height = uint16(zeros(256));
            area.chunk(chk).texture_ids = [];
            area.chunk(chk).texture_blend = uint16(zeros(65));
            area.chunk(chk).color_map = uint16(zeros(65));
            area.chunk(chk).unk1 = [];
            area.chunk(chk).shadow_map = uint8(zeros(65));
        end
    end
    % ##################################
    % DECODE PROPS
    % ##################################
    % 
    if exist('prop')
        % props are separated into a header (list of props) part and a location part.
        nrOfProps = bi2de(props(1:4)',256);
        header = props(5:4+nrOfProps*104);
        % next load the model locations
        modelBuffer = bi2de(reshape(props(4+104*nrOfProps+1:end)',2,[])',256)';
        % model paths are separated by an empty space. the next line finds
        % all model separators
        modelLocations = find(modelBuffer==0);
        % index of the first model
        baseLocation = 4+104*nrOfProps;
        modelLocations = [0 modelLocations];
        modelBuffer = char(modelBuffer);
        % generate a list (can be seen as a dict) of models where the first
        % column tells the location and the second column stores the model
        % path
        for i = 1:length(modelLocations)-1
            models{i} = [{baseLocation + modelLocations(i)*2} {modelBuffer(modelLocations(i)+1:modelLocations(i+1)-1)}];
        end
        % iterate through props
        for prp = 1:nrOfProps
            prop = header(1:104);
            header(1:104) = [];
            % check function for extra details. it extracts data from the
            % header and adds it to a prop struct
            prop = extractPropData(prop,models);
            
            area.prop(prp).id = prop.propId;
            area.prop(prp).unk1 = prop.unk1;
            area.prop(prp).modelOffset = prop.modelLocation;
            area.prop(prp).unk2 = prop.unk2;
            area.prop(prp).scale = prop.scale;
            area.prop(prp).rotation = prop.quarts;
            area.prop(prp).position = prop.XYZ;
            area.prop(prp).unk3 = prop.unk3;
            area.prop(prp).unk4 = prop.unk4;
            area.prop(prp).model = prop.model;
        end
    end
end

function [out] = extractPropData(prop, models)
out = struct();
out.propId = bi2de(reshape(prop(1:4), 4, [])', 256);
out.unk1 = bi2de(reshape(prop(5:20), 4, [])', 256);
% next line is very important. it tetlls us where the model path can be found.
out.modelLocation = bi2de(reshape(prop(21:24), 4, [])', 256);
out.unk2 = bi2de(reshape(prop(25:28), 2, [])', 256);
out.scale = typecast( uint32( bi2de(reshape(prop(29:32), 4, [])', 256) ), 'single');
out.quarts = typecast( uint32( bi2de(reshape(prop(33:48), 4, [])', 256) ), 'single');
out.XYZ = typecast( uint32( bi2de(reshape(prop(49:60), 4, [])', 256) ), 'single');
out.unk3 = bi2de(reshape(prop(61:68), 2, [])', 256);
out.unk4 = bi2de(reshape(prop(69:end), 2, [])', 256);
out.model = '';
for j = 1:length(models)
    if models{j}{1} == out.modelLocation
        out.model = models{j}{2};
        break
    end
end
end