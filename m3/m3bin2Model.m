
utils = utils;

close all
file = 'Rowsdower.m3';

% #######################################
% LOAD FILE
% #######################################
Model = struct();
fid = fopen(file);
data = fread(fid);
fclose(fid);

% #######################################
% READ HEADER
% #######################################
headerSize = 1696;

header = data(1:headerSize);
data(1:headerSize) = [];

headerStruct.magic = char(header(1:4)');
header(1:4) = [];
headerStruct.version = bi2de(header(1:4)',256);
header(1:4) = [];
headerStruct.nameFile = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.nrLetters = bi2de(header(1:8)',256);
header(1:8) = [];

headerStruct.unk05 = bi2de(header(1:8)',256);
header(1:8) = [];

headerStruct.nr15 = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofset15 = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.nr2 = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofset2 = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.nr20 = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofset20 = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.unk2 = bi2de(header(1:8)',256);
header(1:8) = [];
header(1:72) = [];
headerStruct.nr21 = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofset21 = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.nr22 = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofset22 = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.unk25 = bi2de(reshape(header(1:208),8,[])',256);
header(1:208) = [];

headerStruct.nrAnimation = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofsetAnimation = bi2de(header(1:8)',256);
header(1:8) = [];

headerStruct.nr4 = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofset4 = bi2de(header(1:8)',256);
header(1:8) = [];

headerStruct.nr5 = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofset5 = bi2de(header(1:8)',256);
header(1:8) = [];

headerStruct.boneId_nr = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.boneId_ofset = bi2de(header(1:8)',256);
header(1:8) = [];

headerStruct.textureNr = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.textureOfset = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.unk3 = bi2de(reshape(header(1:32),8,[])',256);
header(1:32) = [];

headerStruct.nrMaterials = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofsetMaterials = bi2de(header(1:8)',256);
header(1:8) = [];

headerStruct.unk4 = bi2de(reshape(header(1:32),8,[])',256);
header(1:32) = [];
headerStruct.nrVertex = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofsetVertex = bi2de(header(1:8)',256);
header(1:8) = [];

headerStruct.nrPoly = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofsetPoly = bi2de(header(1:8)',256);
header(1:8) = [];

headerStruct.nrSubmesh = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofsetSubmesh = bi2de(header(1:8)',256);
header(1:8) = [];

headerStruct.unk45 = bi2de(reshape(header(1:32),8,[])',256);
header(1:32) = [];
headerStruct.nrVertexId = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.ofsetVertexId = bi2de(header(1:8)',256);
header(1:8) = [];
headerStruct.unk5 = bi2de(reshape(header(1:280),8,[])',256);
header(1:280) = [];
headerStruct.unk6 = bi2de(reshape(header(1:32),8,[])',256);
header(1:32) = [];
headerStruct.unk11 = bi2de(reshape(header(1:end),4,[])',256);

Model.header = headerStruct;

% #######################################
% EXTRACT DATA FROM STREAM
% #######################################
Model.croppedData.Vertex = data(Model.header.ofsetVertex+1:Model.header.ofsetVertex+84*Model.header.nrVertex);
Model.croppedData.Polygons = data(Model.header.ofsetPoly+1:Model.header.ofsetPoly+Model.header.nrPoly*4);
Model.croppedData.Submesh = data(Model.header.ofsetSubmesh+1:Model.header.ofsetSubmesh+60*Model.header.nrSubmesh);
Model.croppedData.unk2 = data(Model.header.ofset2+1:Model.header.ofset2+Model.header.nr2*2);
Model.croppedData.filename_maybe = data(1:Model.header.nrLetters*2);
Model.croppedData.animationHeader = data(Model.header.ofsetAnimation+1:Model.header.ofsetAnimation+Model.header.nrAnimation*176*2);
Model.croppedData.animation = data(Model.header.ofsetAnimation+Model.header.nrAnimation*176*2+1:Model.header.ofset4);
Model.croppedData.BoneIds_maybe = data(Model.header.boneId_ofset+1:Model.header.boneId_ofset+Model.header.boneId_nr*2);
if Model.header.ofset5>0     % this is some strange data. it seems to store bone ids in a specific order.
    Model.croppedData.bone_related_maybe = data(Model.header.ofset5+1:Model.header.ofset5+Model.header.nr5*2);
end
Model.croppedData.VertexIds = data(Model.header.ofsetVertexId+1:Model.header.ofsetVertexId+Model.header.nrVertexId*4);

% #######################################
% DECODE DATA
% #######################################
% VERTICES
% next line creates an (nrVertex x 84) matrix. all vertex data is stored in
% this matrix
vertices = reshape(Model.croppedData.Vertex,84,[])';
for i = 1:size(vertices,1)
    % next line takes the first vertex and reshapes the (84 x 1) matrix to
    % an (21 x 4) matrix for easier data manipulation
    v = reshape(vertices(i,:),4,[])';
    % next 3 lines extract the xyz positions of the vertex
    vx = typecast(uint8(v(1,:)), 'single');
    vy = typecast(uint8(v(2,:)), 'single');
    vz = typecast(uint8(v(3,:)), 'single');
    % next 3 lines are skipped becuse they contain unknown xyz properties
    % unk1x = typecast(uint8(v(4,:)), 'single');
    % unk1y = typecast(uint8(v(5,:)), 'single');
    % unk1z = typecast(uint8(v(6,:)), 'single');
    % lines 7,8 and 9 contain normal xyz data (maybe?)
    nx = typecast(uint8(v(7,:)), 'single');
    ny = typecast(uint8(v(8,:)), 'single');
    nz = typecast(uint8(v(9,:)), 'single');
    % next 3 lines are skipped becuse they contain unknown xyz properties
    % unk2x = typecast(uint8(v(10,:)), 'single');
    % unk2y = typecast(uint8(v(11,:)), 'single');
    % unk2z = typecast(uint8(v(12,:)), 'single');
    
    % next line contain bone ids for the vertex (1x4) matrix
    bone_id = v(13,:);
    % next line contain bone assigned weights for the bones above (1x4) matrix
    % to check, the sum of all weights must equal to 255
    weight = v(14,:);
    
    % unknown line
    % unk3x = typecast(uint8(v(15,:)), 'single');
    % texture uv
    tu = typecast(uint8(v(16,:)), 'single');
    tv = 1-typecast(uint8(v(17,:)), 'single');
    
    % generate relevant matrices for data extracted from the vertex
    Model.Vertices(i,:) = [vx vy vz];
    Model.Textures(i,:) = [tu tv];
    Model.Normals(i,:) = [nx ny nz];
    Model.Bones(i,:) = bone_id;
    Model.Weights(i,:) = weight;
end

% FACES
Model.Polys = bi2de(reshape(Model.croppedData.Polygons,4,[])', 256);
Model.Faces = reshape(Model.Polys,3,[])';

% OTHER EXTRACTED DATA THAT IS OF LITTLE USE
Model.bone_related = bi2de(reshape(Model.croppedData.bone_related_maybe,2,[])',256)';
Model.BoneIds = bi2de(reshape(Model.croppedData.BoneIds_maybe,2,[])',256)';
Model.fileName = char(bi2de(reshape(Model.croppedData.filename_maybe,2,[])',256));
Model.data2 = bi2de(reshape(Model.croppedData.unk2,2,[])',256)';
Model.VertexIds = bi2de(reshape(Model.croppedData.VertexIds,4,[])',256)';
% EXTRA UNKNOWN DATA
% Model.data15 = reshape(bi2de(reshape(data(Model.ofset15+1:Model.ofset15+Model.nr15*56),2,[])',256)',56,[]);

% if Model.ofset20>0
%     Model.data20 = bi2de(reshape(data(Model.ofset20+1:Model.ofset20+Model.nr20*4),4,[])',256);
% end
% if Model.ofset21>0
% Model.data21 = reshape(bi2de(reshape(data(Model.ofset21+1:Model.ofset21+Model.nr21*2*24),2,[])',256),24,[]);
% end
% if Model.ofset22>0
% Model.data22 = bi2de(reshape(data(Model.ofset22+1:Model.ofset22+Model.nr22*2),2,[])',256)';
% end



% #######################################
% ANIMATIONS
% #######################################
% prepare animation headers for extracting animations
% next line generates a (176*2 x nr_bones) matrix
animationHeaders = reshape(Model.croppedData.animationHeader,176*2,[])';
animationData = Model.croppedData.animation;
for i = 1:Model.header.nrAnimation
    % get first bone header data
    temp = animationHeaders(i,:);
    % READ ANIMATION/BONE HEADER
    Model.anim(i).header.unk1 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).bone_id = i-1;
    Model.anim(i).parent_id = bi2de(temp(1:4),256);
    temp(1:4) = [];
    
    %     Model.anim(i).header.unk20 = bi2de(temp(1:1),256);
    %     temp(1:1) = [];
    %     Model.anim(i).header.unk21 = bi2de(temp(1:1),256);
    %     temp(1:1) = [];
    %     Model.anim(i).header.unk22 = bi2de(temp(1:1),256);
    %     temp(1:1) = [];
    %     Model.anim(i).header.unk23 = bi2de(temp(1:1),256);
    %     temp(1:1) = [];
    %     Model.anim(i).header.unk25 = bi2de(temp(1:4),256);
    %     temp(1:4) = [];
    Model.anim(i).header.unk21 = bi2de(temp(1:2),256);
    temp(1:2) = [];
    Model.anim(i).header.unk22 = bi2de(temp(1:2),256);
    temp(1:2) = [];
    Model.anim(i).header.unk25 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    
    Model.anim(i).header.nr1 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.start1 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.end1 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    
    Model.anim(i).header.nr2 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.start2 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.end2 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    
    Model.anim(i).header.nr3 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.start3 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.end3 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    
    Model.anim(i).header.nr4 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.start4 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.end4 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    
    % THIS IS WHERE(MOST LIKLEY) THE BONE ANIMATION TRANSLATION DATA IS LOCATED
    Model.anim(i).header.nr5 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.start5 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.end5 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    
    Model.anim(i).header.nr6 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.start6 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.end6 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    
    % THIS IS WHERE(MOST LIKLEY) THE BONE ANIMATION ROTATION DATA IS LOCATED
    % SEEMS TO BE IN QUATERNIONS
    Model.anim(i).header.nr7 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.start7 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.end7 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    
    Model.anim(i).header.nr8 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.start8 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    Model.anim(i).header.end8 = bi2de(temp(1:8),256);
    temp(1:8) = [];
    
    Model.anim(i).header.nr9 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.start9 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.end9 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.nr10 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.start10 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.end10 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.nr11 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.start11 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.end11 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.nr12 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.start12 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.end12 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    
    Model.anim(i).header.nr13 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.start13 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.end13 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.nr14 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.start14 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.end14 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.nr15 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.start15 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.end15 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.nr16 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.start16 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.end16 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.nr17 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.start17 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.end17 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    
    Model.anim(i).header.nr18 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.start18 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.end18 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.nr19 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.start19 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    Model.anim(i).header.end19 = bi2de(temp(1:4),256);
    temp(1:4) = [];
    
    Model.anim(i).header.unk3 = bi2de(reshape(temp(1:end),2,[])',256);
    anim = Model.anim(i).header;
    
    % READ ROTATION DATA
    if anim.nr5 ~= 0
        start = Model.anim(i).header.start5;
        % extract timestamp values (milliseconds)
        tempAnimationData = animationData(start+1:start+Model.anim(i).header.nr5*4);
        Model.anim(i).rot.timestamp = bi2de(reshape(tempAnimationData,4,[])',256); % timestamp
        start = Model.anim(i).header.end5;
        % extract rotation values (quaternions)
        tempAnimationData = animationData(start+1:start+Model.anim(i).header.nr5*4*4);
        Model.anim(i).rot.value = reshape(typecast(uint8(tempAnimationData),'single'),4,[])'; % rotation
    end
    % READ TRANSLATION DATA
    if anim.nr7 ~= 0
        start = Model.anim(i).header.start7;
        % extract timestamp values (milliseconds)
        tempAnimationData = animationData(start+1:start+Model.anim(i).header.nr7*4);
        Model.anim(i).tra.timestamp = bi2de(reshape(tempAnimationData,4,[])',256); % timestamp
        start = Model.anim(i).header.end7;
        % extract translation values (xyz)
        tempAnimationData = animationData(start+1:start+Model.anim(i).header.nr7*4*3);
        Model.anim(i).tra.value = reshape(typecast(uint8(tempAnimationData),'single'),3,[])'; % translation
    end
end

% connect skeleton
for i = 1:Model.header.nrAnimation
    % get first bone header data
    temp = animationHeaders(i,:);
    temp = reshape(temp,4,[])';
    Model.anim(i).bonePos = typecast(uint8([temp(52+33,:) temp(52+34,:) temp(52+35,:)]), 'single');
    try
        Model.anim(i).parentBone = Model.anim(Model.anim(i).parent_id+1).bonePos;
    catch  
        % this is a hack i implemented. seems to be working but extra bones
        % do appear. another solution could be to use:
        % Model.anim(Model.anim(i).parent_id+1).bonePos
        Model.anim(i).parentBone = [0 0 0];
    end
end

submesh = reshape(Model.croppedData.Submesh,60,[])';
for i = 1:Model.header.nrSubmesh
    sub_mesh =  submesh(i,:);
    Model.Submeshes(i).Material = bi2de(sub_mesh(23:24),256);
    sub_mesh = bi2de(reshape(sub_mesh, 4, [])',256);
    Model.Submeshes(i).Indices = sub_mesh(3);
    Model.Submeshes(i).Vertices = sub_mesh(4);
end

nV = 1;
nI = 1;
for i = 1:Model.header.nrSubmesh
    Model.Submeshes(i).Name = ['model_' num2str(i)];
    Model.Submeshes(i).V = Model.Vertices(nV:nV+Model.Submeshes(i).Vertices-1,:);
    Model.Submeshes(i).I = reshape(Model.Polys(nI:nI+Model.Submeshes(i).Indices-1),3,[])';
    Model.Submeshes(i).T = Model.Textures(nV:nV+Model.Submeshes(i).Vertices-1,:);
    Model.Submeshes(i).N = Model.Normals(nV:nV+Model.Submeshes(i).Vertices-1,:);
    Model.Submeshes(i).BI = Model.Bones(nV:nV+Model.Submeshes(i).Vertices-1,:);
    Model.Submeshes(i).BW = Model.Weights(nV:nV+Model.Submeshes(i).Vertices-1,:);
    Model.Submeshes(i).M = Model.Submeshes(i).Material;
    nV = nV + Model.Submeshes(i).Vertices;
    nI = nI + Model.Submeshes(i).Indices;
end

%PLOTS
% utils.plotSkeleton(Model);
% utils.plotMesh(Model);
% utils.plotSubMesh(Model, 10);
% utils.plotVertexPerBone(Model, 10);
% utils.plotVertexWeightsToBones(Model);
% utils.animate(Model,0,70)


%texture
Model.data.texture = data(Model.header.textureOfset+1:Model.header.textureOfset+Model.header.textureNr*32);
for i = 1:Model.header.textureNr
    Model.texture(i).unk1 = bi2de(Model.data.texture(1:2)',256);
    Model.texture(i).id = bi2de(Model.data.texture(3:4)',256);
    Model.texture(i).unk2 = bi2de(reshape(Model.data.texture(5:16),2,[])',256);
    Model.texture(i).length = bi2de(Model.data.texture(17:24)',256);
    Model.texture(i).ofset = bi2de(Model.data.texture(25:32)',256);
    start = Model.header.textureOfset+Model.header.textureNr*32;
    Model.data.texture(1:32) = [];
    Model.texture(i).location = char(bi2de(reshape(data(start+Model.texture(i).ofset+1:start+Model.texture(i).ofset+Model.texture(i).length*2),2,[])',256))';
    last = start+Model.texture(i).ofset+Model.texture(i).length*2;
end
Model.data.texture = data(Model.header.textureOfset+1:last);

%Materials Might contain errors
materials = data(Model.header.ofsetMaterials+1:Model.header.ofsetMaterials+Model.header.nrMaterials*48);
start = Model.header.ofsetMaterials+Model.header.nrMaterials*48;
for i = 1:Model.header.nrMaterials
    Model.material(i).unk1 = bi2de(materials(1:20)', 256);
    Model.material(i).id = bi2de(materials(21:24)', 256);
    Model.material(i).unk2 = bi2de(materials(25:28)', 256);
    Model.material(i).unk3 = bi2de(materials(29:32)', 256);
    Model.material(i).nr = bi2de(materials(33:40)', 256);
    Model.material(i).ofset = bi2de(materials(41:48)', 256);
    Model.material(i).data = data(start+Model.material(i).ofset+1:start+Model.material(i).ofset+304);
    materials(1:48) = [];
    last = start+Model.material(i).ofset+304;
end

