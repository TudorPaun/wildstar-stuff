function out = utils
    out.plotSkeleton = @plotSkeleton;
    out.plotMesh = @plotMesh;
    out.plotSubMesh = @plotSubMesh;
    out.plotVertexPerBone = @plotVertexPerBone;
    out.plotVertexWeightsToBones = @plotVertexWeightsToBones;
    out.animate = @animate;
end

function plotSkeleton(Model)
bonePos = [];
boneParents = [];
txt = [];
figure
for i = 2:length(Model.anim)
    if any(Model.anim(i).bonePos~=0)
        bonePos = [bonePos; Model.anim(i).bonePos];
        boneParents = [boneParents; Model.anim(i).parentBone];
        txt = [txt; double(Model.anim(i).parentBone-(Model.anim(i).parentBone - Model.anim(i).bonePos)/2)  i-1];
    end
end
bones = (boneParents-bonePos)/2;
bones = boneParents - bones;
plot3([bonePos(:,1) boneParents(:,1)]', [bonePos(:,2) boneParents(:,2)]', [bonePos(:,3) boneParents(:,3)]', 'LineWidth', 5)

for i = 1:size(txt,1)
    text(txt(i,1),txt(i,2),txt(i,3), num2str(txt(i,4)), 'FontSize',14)
end
end

function plotMesh(Model)
Lines1 = [];
Lines2 = [];
Lines3 = [];
for i = 1:length(Model.Submeshes)
    for j = 1:size(Model.Submeshes(i).I,1)
        polygons = Model.Submeshes(i).I(j,:);
        v1 = Model.Submeshes(i).V(polygons(1)+1,:);
        v2 = Model.Submeshes(i).V(polygons(2)+1,:);
        v3 = Model.Submeshes(i).V(polygons(3)+1,:);
        Lines1 = [Lines1 [v1(1) v2(1) v3(1); v2(1) v3(1) v1(1)]];
        Lines2 = [Lines2 [v1(2) v2(2) v3(2); v2(2) v3(2) v1(2)]];
        Lines3 = [Lines3 [v1(3) v2(3) v3(3); v2(3) v3(3) v1(3)]];
    end
end
plot3(Lines1, Lines2, Lines3, 'b')
end

function plotSubMesh(Model, marker_size)
figure
hold on
colors = jet(length(Model.Submeshes));
last_idx = 0;
for i = 1:length(Model.Submeshes)
    V1 = [];
    V2 = [];
    V3 = [];
    list = [];
    for j = 1:size(Model.Submeshes(i).I,1)
        list = [list Model.Submeshes(i).I(j,:)];
    end
    list = unique(list);
    for j = 1:length(list)
        V1 = [V1 Model.Vertices(list(j)+1+last_idx,1)];
        V2 = [V2 Model.Vertices(list(j)+1+last_idx,2)];
        V3 = [V3 Model.Vertices(list(j)+1+last_idx,3)];
    end
    last_idx = last_idx + length(list);
    plot3(V1, V2, V3, 'o', 'MarkerFaceColor', colors(i,:), 'MarkerSize',marker_size)
    pause(0.01)
end
axis equal
end

function plotVertexPerBone(Model, marker_size)
figure
hold on
bone_list = unique(Model.Bones);
colors = jet(length(bone_list));
for i = 1:length(bone_list)
    vertices_to_bone = sum(Model.Bones==bone_list(i),2);
    verts = Model.Vertices(vertices_to_bone==1, :);
    plot3(verts(:,1), verts(:,2), verts(:,3), 'o', 'MarkerFaceColor', colors(i,:), 'MarkerSize',marker_size)
end
axis equal
end

function plotVertexWeightsToBones(Model)
plotSkeleton(Model)
hold on;
bonePos = [];
for i = 2:length(Model.anim)
    bonePos = [bonePos; Model.anim(i).bonePos];
end
bone_list = unique(Model.Bones);
bone_list(1) = [];
colors = jet(length(bone_list));
for i = 1:length(bone_list)
    Lines1 = [];
    Lines2 = [];
    Lines3 = [];
    vertices_to_bone = sum(Model.Bones==bone_list(i),2);
    verts = Model.Vertices(vertices_to_bone==1, :);
    plot3(verts(:,1), verts(:,2), verts(:,3), 'o', 'MarkerFaceColor', colors(i,:), 'MarkerSize',4)
    for j=1:size(verts,1)
        Lines1 = [Lines1 [verts(j,1); bonePos(bone_list(i),1)]];
        Lines2 = [Lines2 [verts(j,2); bonePos(bone_list(i),2)]];
        Lines3 = [Lines3 [verts(j,3); bonePos(bone_list(i),3)]];
    end
    plot3(Lines1, Lines2, Lines3, 'b', 'color',colors(i,:))
end
axis equal
end

function animate(Model, start_time, end_time)
keyframes = [];
for i = 1:length(Model.anim)
    if ~isempty(Model.anim(i).tra)
        keyframes = [keyframes Model.anim(i).tra.timestamp'];
    end
end
keyframes = unique(keyframes);
keyframes(keyframes<start_time*1000) = [];
keyframes(keyframes>end_time*1000) = [];

skeleton = [];
for i = 1:length(Model.anim)
    if ~isempty(Model.anim(i).parentBone)
        if all(Model.anim(i).bonePos~=0)
            skeleton = [skeleton; [i Model.anim(i).header.id+1]];
        end
    end
    bones{i} = Model.anim(i).bonePos;
end
model = [];
for i = 1:size(skeleton,1)
    model = [model; [bones{skeleton(i,1)} bones{skeleton(i,2)}]];
end

plot3(model(:,[1,4])',model(:,[2,5])',model(:,[3,6])', 'LineWidth', 5)
axis equal

for i = 1:length(keyframes)
    for j = 1:length(Model.anim)
        if ~isempty(Model.anim(j).tra)
            key = (Model.anim(j).tra.timestamp == keyframes(i));
            if j==17
                if sum(key)>0
                    bones{j} = Model.anim(j).tra.value(key,:);
                end
            end
        end
    end
    model = [];
    for j = 1:size(skeleton,1)
        model = [model; [bones{skeleton(j,1)} bones{skeleton(j,2)}]];
    end
    plot3(model(:,[1,4])',model(:,[2,5])',model(:,[3,6])', 'LineWidth', 5);
    drawnow
    pause(0.01)
end
end